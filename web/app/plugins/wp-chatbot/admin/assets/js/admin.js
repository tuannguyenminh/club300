
jQuery(document).ready(function($){
    
    // for support wp-color-picker
    $('.htcc-color-wp').wpColorPicker();

    // $('select').material_select();
    // $('select').formSelect();   // v1.0.0.rc.2

    // $('.collapsible').collapsible();

});


/**
 * makes an ajax call
 * by default service_content will hide using style="display: none;"
 * if ht_cc_service_content option is not set or not equal to hide
 * then show the card  - set display: block
 * ajax action at admin.php
 */
jQuery.post(    
    ajaxurl, 
    {
        'action': 'ht_cc_service_content',
    }, 
    function(response){
        if ( 'hide' !== response ) {
            var service_content = document.querySelector(".service-content");
            if ( service_content ) {
                service_content.style.display = "block";
            }
        }
    }
);
  
  
/**
 * when clicked on hide at admin - service content
 * makes an ajax call an update / create the ht_cc_service_content option to hide
 * ajax action at admin.php
 */
function ht_cc_admin_hide_services_content() {

jQuery.post(
    ajaxurl, 
    {
        'action': 'ht_cc_service_content_hide',
    }, 
);

var service_content = document.querySelector(".service-content");

if  ( service_content ) {
    service_content.style.display = "none";
}

}