<?php
/**
 * Admin  - enqueue sytle, scripts
 */


if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'HTCC_Enqueue' ) ) :

class HTCC_Enqueue {


    function enqueue( $hook ) {

        // echo $hook;
        if( 'toplevel_page_wp-chatbot' == $hook || 'wp-chatbot_page_wp-chatbot-actions' == $hook || 'wp-chatbot_page_wp-chatbot-pro-woo' == $hook ) {

            wp_enqueue_style( 'wp-color-picker' );

            wp_enqueue_style( 'htcc_admin_styles', plugins_url( 'admin/assets/css/admin-styles.css', HTCC_PLUGIN_FILE ), '', HTCC_VERSION );
            wp_enqueue_style( 'htcc_admin_md_styles', plugins_url( 'admin/assets/css/materialize.min.css', HTCC_PLUGIN_FILE ), '', HTCC_VERSION );

            wp_enqueue_script( 'htcc_md_js', plugins_url( 'admin/assets/js/prev_md.js', HTCC_PLUGIN_FILE ), array( 'jquery', 'wp-color-picker' ), HTCC_VERSION );
            wp_enqueue_script( 'htcc_js', plugins_url( 'admin/assets/js/admin.js', HTCC_PLUGIN_FILE ), array( 'jquery', 'htcc_md_js', 'wp-color-picker' ), HTCC_VERSION );


        }

    }

}

$htcc_enqueue = new HTCC_Enqueue();

add_action('admin_enqueue_scripts', array( $htcc_enqueue, 'enqueue' ) );


endif; // END class_exists check